# parcial 4

Proyecto de Flutter para parcial #4

## Datos

Alumno: Gerardo Alexis Flores Mejía

Carnet: 25-0036-2017

## Captura de Pantallas del funcionamiento

<img src="./capturas/1.jpeg" width="200">
<img src="./capturas/2.jpeg" width="200">
<img src="./capturas/3.jpeg" width="200">
<img src="./capturas/4.jpeg" width="200">
<img src="./capturas/5.jpeg" width="200">
<img src="./capturas/6.jpeg" width="200">
