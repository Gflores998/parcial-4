import 'dart:async';

import 'package:provider/provider.dart';

import 'package:flutter/material.dart';
import 'package:parcial_4/providers/theme_provider.dart';
import 'package:parcial_4/pages/layout.dart';
import 'package:parcial_4/utils/color.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
    create: (context) => ThemeProvider(),
    builder: (context, _) {
      // Proveedor para obtener el tema del móvil
      final themeProvider = Provider.of<ThemeProvider>(context);

      // ignore: prefer_const_constructors
      return MaterialApp(
          title: 'Parcial 4',
          debugShowCheckedModeBanner: false,
          themeMode: themeProvider.themeMode,
          theme: MyCustomColors.lightTheme,
          darkTheme: MyCustomColors.darkTheme,
          home: LayoutPage()
        );
    },
  );
}
