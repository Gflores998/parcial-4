import 'package:flutter/material.dart';

class InformacionPage extends StatefulWidget {
  const InformacionPage({ Key? key }) : super(key: key);

  @override
  _InformacionPageState createState() => _InformacionPageState();
}

class _InformacionPageState extends State<InformacionPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'Alumno: Gerardo Alexis Flores Mejía\n',
                      style: TextStyle(color:Theme.of(context).primaryColor)
                    ),
                    TextSpan(
                      text: 'Carnet: 25-0036-2017',
                      style: TextStyle(color:Theme.of(context).primaryColor)
                    )
                  ]
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}