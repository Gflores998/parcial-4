import 'package:flutter/material.dart';
import 'package:parcial_4/pages/informacion.dart';
import 'package:parcial_4/pages/ubicaciones.dart';

class LayoutPage extends StatefulWidget {
  const LayoutPage({ Key? key }) : super(key: key);

  @override
  _LayoutPageState createState() => _LayoutPageState();
}

class _LayoutPageState extends State<LayoutPage> {

  var listaPaginas = [
    const UbicacionesPage(),
    const InformacionPage()
  ];
  int indexPagina = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0),
        child: AppBar(
            centerTitle: true,
            title: Text('Parcial 4', style: TextStyle(color:Theme.of(context).primaryColor),),
        ),
      ),
      body: listaPaginas[indexPagina],
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 20,
        currentIndex: indexPagina,
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: true,
        selectedFontSize: 10,
        unselectedFontSize: 10,
        onTap: (int cPage) {
          setState(() {
            indexPagina = cPage;
          });
        },
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.map_sharp), label: 'Ubicaciones'),
          BottomNavigationBarItem(icon: Icon(Icons.inbox), label: 'Información'),
        ],
      ),
    );
  }
}