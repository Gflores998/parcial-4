import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


class UbicacionesPage extends StatefulWidget {
  const UbicacionesPage({ Key? key }) : super(key: key);

  @override
  _UbicacionesPageState createState() => _UbicacionesPageState();
}

class _UbicacionesPageState extends State<UbicacionesPage> {
  List<Marker> markers =[];

  String _darkMapStyle = '';
  String _lightMapStyle = '';

  Completer<GoogleMapController> _controller = Completer();
  
  void llenarMarcadores() {
    Marker etiquetauno = Marker(
      markerId: MarkerId('Sucursal Lourdes Colón'),
      position:LatLng(13.72467010769968, -89.36885148950012), 
      infoWindow: InfoWindow(
        title: 'Encomiendas SV Sucursal Lourdes Colón',
        snippet: 'Encuentranos en el centro de Lourdes Colón'
      ),
      icon: BitmapDescriptor.defaultMarkerWithHue(
        BitmapDescriptor.hueBlue
      ),
    );

    Marker etiquetados = Marker(
      markerId: MarkerId('Sucursal Merliot'),
      position:LatLng( 13.680677148437244, -89.27145281657893 ),
      infoWindow: InfoWindow(
        title: 'Encomiendas SV Sucursal Ciudad Merliot',
        snippet: 'Encuentranos en el centro de Ciudad Merliot'
      ),
      icon: BitmapDescriptor.defaultMarkerWithHue(
      BitmapDescriptor.hueMagenta
      ),
    );

    Marker etiquetatres = Marker(
      markerId: MarkerId('Sucursal Santa Tecla'),
      position:LatLng(13.677671528200305, -89.28953664874545),
      infoWindow: InfoWindow(
        title: 'Encomiendas SV Sucursal Santa Tecla',
        snippet: 'Encuentranos en el centro de Santa Tecla',
      ),
      icon: BitmapDescriptor.defaultMarkerWithHue(
      BitmapDescriptor.hueGreen
      ),
    );

    Marker etiquetaCuatro = Marker(
      markerId: MarkerId('Sucursal San Vicente'),
      position:LatLng(13.640669993574242, -88.78306328739967),
      infoWindow: InfoWindow(
        title: 'Encomiendas SV Sucursal San Vicente',
        snippet: 'Encuentranos en el centro de San Vicente'
      ),
      icon: BitmapDescriptor.defaultMarkerWithHue(
      BitmapDescriptor.hueOrange
      ),
    );

    setState(() {
      markers.add(etiquetauno);
      markers.add(etiquetados);
      markers.add(etiquetatres);
      markers.add(etiquetaCuatro);
    });
  }

  @override
  void initState(){
    super.initState();

    llenarMarcadores();
  }

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(13.68935, -89.18718),
    zoom: 14.4746,
  );

  @override
  Widget build(BuildContext context) {

    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: _kGooglePlex,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      markers: markers.map((mark) => mark).toSet(),
    );
  }
}