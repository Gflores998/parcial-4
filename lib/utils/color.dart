import 'package:flutter/material.dart';

class MyCustomColors {
  
  static final lightTheme = ThemeData.light().copyWith(
    backgroundColor: Color(0xFFFFFFFF),
    scaffoldBackgroundColor: Color(0xFFFFFFFF),
    appBarTheme: AppBarTheme(
      backgroundColor: Color(0xFFF8F5FA)
    ),
    primaryColor: Color(0xFF936BAF),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Color(0xFFF8F5FA),
      selectedItemColor: Color(0xFFF37F2A)
    )
  );

  static final darkTheme = ThemeData.dark().copyWith(
    backgroundColor: Color(0xFF120b21),
    scaffoldBackgroundColor: Color(0xFF120b21),
    appBarTheme: AppBarTheme(
      backgroundColor: Color(0xFF190f2d)
    ),
    primaryColor: Color(0xFFC2B7DA),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Color(0xFF190f2d),
      selectedItemColor: Color(0xFFFFEE86)
    )
  );
}
